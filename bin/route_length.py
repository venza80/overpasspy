#!/usr/bin/python

import sys

import overpass.relations
import overpass.ways
import overpass.qa.ways
import overpass

if len(sys.argv) < 2:
    print("Usage: {0} <line id>".format(sys.argv[0]))
    sys.exit(1)

overpass.init()

print("Querying OSM...")
route = overpass.relations.get(sys.argv[1])
ordered_way_ids = [x["ref"] for x in route["members"] if x["type"] == "way" and x["role"] == ""]
print("Line:", route["tags"]["name"])
members = overpass.relations.get_members(sys.argv[1], ["node", "way"])
ways = dict([(x["id"], x) for x in members if x["id"] in ordered_way_ids])

connected = overpass.qa.ways.is_connected(ways.values())
if len(connected) > 0:
    print("Route is not connected, length will be wrong")

lengths = []
for way_id in ordered_way_ids:
    way = ways[way_id]
#    print("Way {0} has {1} nodes".format(way["id"], len(nodes)))
    d = overpass.ways.length_km(way)
    lengths.append(d)
#    print("--> %.2fKm" % (d,))

print("Total: %.2fKm" % sum(lengths))
overpass.end()

