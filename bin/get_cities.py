#!/usr/bin/python

import json
#from pprint import pprint

import overpass.execute
import overpass.debug
import overpass.nodes

overpass.init()

print("Querying OSM...")
#cities = overpass.execute.execute_QL("node(36.01,-9.73,43.29,3.93)[place=\"city\"];")
cities = overpass.execute.execute_QL("node[place=\"city\"];out;")
print(cities)

overpass.end()

json.dump(cities, open("all_cities.json", "w"))
