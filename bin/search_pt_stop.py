#!/usr/bin/python

import sys
import overpass.public_transport
import overpass

if len(sys.argv) < 2:
    print("Usage: {0} <stop name>".format(sys.argv[0]))
    sys.exit(1)

overpass.init()

print("Querying OSM for public transport stops called {0}".format(sys.argv[1]))

stops = overpass.public_transport.get_all_bus_stops_fuzzy(sys.argv[1])

print("Results:")

for stop in stops:
    print("Name:", stop["tags"]["name"])
    print("ID:", stop["id"])
    print("Where:", overpass.nodes.is_in(stop["id"]))
    print("-----------")

overpass.end()

