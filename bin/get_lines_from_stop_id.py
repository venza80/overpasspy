#!/usr/bin/python

import sys

import overpass.nodes
import overpass

if len(sys.argv) < 2:
    print("Usage: {0} <stop id>".format(sys.argv[0]))
    sys.exit(1)

overpass.init()

print("Querying OSM...")
routes = overpass.nodes.get_parent_relations(sys.argv[1])
routes = sorted([x for x in routes if x["tags"]["type"] == "route"], key=lambda x: x["tags"]["name"])
for route in routes:
    print("Name:", route["tags"]["name"])
    print("ID:", route["id"])
    print("-------------------")

overpass.end()
