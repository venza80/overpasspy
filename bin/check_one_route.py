#!/usr/bin/python3

import sys
import time
from datetime import datetime, timezone
#from pprint import pprint

import overpass.relations
import overpass.ways
import overpass.qa.ways
import overpass.qa.public_transport
import overpass.public_transport
import overpass.highways
import overpass.debug

if len(sys.argv) < 2:
    print("Usage: {0} <route id>".format(sys.argv[0]))
    sys.exit(1)

start_time = time.time()

overpass.init()

print("Querying OSM...")
route = overpass.relations.get(sys.argv[1])
issues = {}
ordered_way_ids = [x["ref"] for x in route["members"] if x["type"] == "way" and x["role"] == ""]
try:
    print("Line {0} ({1})".format(route["tags"]["name"], route["id"]))
except KeyError:
    print("Line with ID {0}".format(route["id"]))
    issues["route_has_no_name"] = True
members = overpass.relations.get_members(route["id"], ["node", "way"])
ways = dict([(x["id"], x) for x in members if x["id"] in ordered_way_ids])

stops_check = overpass.qa.public_transport.check_route_has_stops(route)
if stops_check is not None:
    issues["stops"] = stops_check

old_tagging = overpass.qa.public_transport.check_old_tagging(route)
if old_tagging is not None:
    issues["old_tagging"] = old_tagging

connected = overpass.qa.ways.is_connected(ways.values())
if len(connected) > 0:
    issues["ways_connected"] = connected

# print issues
if len(issues) > 0:
    print("Issues detected:")
    if "ways_connected" in issues:
        print("\t- Contains disconnected ways:")
        for c in connected:
            way_name = overpass.highways.get_name(c[1])
            if overpass.highways.get_name(c[1]) is None:
                print("\t-- Node {0} of way {1}".format(c[0], c[1]["id"]))
            else:
                print("\t-- Node {0} of way {2} ({1})".format(c[0], way_name, c[1]["id"]))
    if "route_has_no_name" in issues:
        print("\t- Route has no name")
    if "stops" in issues:
        print("\t- {0}".format(issues["stops"]))
    if "old_tagging" in issues:
        print("\t- {0}".format(issues["old_tagging"]))
    print("http://127.0.0.1:8111/load_object?new_layer=true&objects=relation{0}".format(route["id"]))
#        pprint(connected)
else:
    print("Route is ok")

print("Execution time: {0:.2f}s".format(time.time() - start_time))

overpass.end()
