OverPassPy
==========

OVerPassPy is a project in early development that aims to provide Python 3 bindings to the
[http://wiki.openstreetmap.org/wiki/Overpass_API](OpenStreeMap Overpass API).

The current development focus is on public transport quality assurance.

The library uses the official, public, Overpass API servers, in particular the default one
is http://overpass.osm.rambler.ru/. If memcached is available, all replies are cached for half
an hour to have quicker response times for faster execution after the first time, this is
useful while fixing bugs in the library, but is also means that edits will not show up for
half an hour at most.

Basic usage - tools
-------------------
In the `bin` subdirectory some tools that use the API can be run. The most interesting is
`check_routes.py`:
```
$ check_routes.py <City/Town name>
```
Will download all public transport lines in the specified city (admin levels 6, 7 and 8
are looked up) and run a number of QA tests on them. An html file named `<City>_qa.html`
will be generated, with links to JOSM for fixing errors.

An example, generated once per day, is here:
[http://rincewind.rlieh.eu/osm/Antibes_qa.html](http://rincewind.rlieh.eu/osm/Antibes_qa.html)

Basic usage - API
-----------------
The overpass Python package comes with several modules and subpackages that need to be
imported when needed. To calculate the length in km of a way, for example, this is the
code required:
```
import overpass
import overpass.distance
import overpass.ways

overpass.init()

way = overpass.ways.get(way_id)
km = overpass.ways.length_km(way)

print("Way %s is %.2fkm long" % (overpass.highways.get_name(way), km))

overpass.end()
```
