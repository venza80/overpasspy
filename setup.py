from setuptools import setup, find_packages

setup(
    name='OverPassPy',
    version='0.1',
    author='Daniele Venzano',
    author_email='venza@brownhat.org',
    packages=find_packages(),
    scripts=[],
    url='http://pypi.python.org/pypi/OverPassPy/',
    license='GPLv2',
    description='Python client for the OpenStreetMap Overpass API',
    long_description=open('README.md').read(),
    test_suite='overpass.test',
    setup_requires=['nose>=1.0'],
    install_requires=[
        "python3-memcached == 1.51",
    ],
    classifiers=[
            'Development Status :: 3 - Alpha',
            'Environment :: Console',
            'Intended Audience :: Developers',
            'Topic :: Scientific/Engineering :: GIS',
            'Topic :: Software Development :: Libraries',
            'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
            'Programming Language :: Python :: 2',
            'Programming Language :: Python :: 3',
    ],
    keywords='osm openstreetmap qa overpass',
)

