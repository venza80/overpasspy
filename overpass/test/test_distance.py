import unittest

import overpass.distance as distance


class DistanceTests(unittest.TestCase):
    def test_haversine(self):
        a = {"lat": 40.7486, "lon": -73.9864}
        b = {"lat": 41.7486, "lon": -74.9864}
        ret = distance.haversine(a, b)
        assert ret == 139.1151913835227

    def test_way_to_km_iterable(self):
        self.assertRaises(TypeError, distance.way_to_km, 5)
