import unittest

import overpass
import overpass.execute

script = 'node["name"="Gielgen"];out;'


class ExecuteTests(unittest.TestCase):
    def setUp(self):
        overpass.init(force_lock=True)

    def tearDown(self):
        overpass.end()

    def test_execute_QL(self):
        data = overpass.execute.execute_QL(script)
        assert len(data["elements"]) > 1

    def test_execute_QL_error(self):
        data = overpass.execute.execute_QL(script + "ggg")
        assert data is None

