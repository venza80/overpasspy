import unittest

import overpass
import overpass.relations


class RelationsTests(unittest.TestCase):
    def setUp(self):
        overpass.init(force_lock=True)

    def tearDown(self):
        overpass.end()

    def test_get(self):
        ret = overpass.relations.get(22377)
        self.assertIsNotNone(ret)
