import logging
log = logging.getLogger(__name__)

import overpass
import overpass.execute


def dump_stats():
    log.debug("Cache statistics:")
    for key in overpass.cache.stats:
        log.debug("  {0}: {1}".format(key, overpass.cache.stats[key]))
    log.debug("Overpass QL statistics:")
    for key in overpass.execute.stats:
        log.debug("  {0}: {1}".format(key, overpass.execute.stats[key]))
