def get_name(highway):
    if "name" in highway["tags"]:
        return highway["tags"]["name"]
    if "ref" in highway["tags"]:
        return highway["tags"]["ref"]
    return None
