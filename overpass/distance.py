import math
from collections import Iterable


def _node_to_coords(node):
    return (node["lat"], node["lon"])


def haversine(node_a, node_b):
    R = 6371  # Earth's radius in Km

    a = _node_to_coords(node_a)
    b = _node_to_coords(node_b)
    lat1 = math.radians(a[0])
    lat2 = math.radians(b[0])
    lon1 = math.radians(a[1])
    lon2 = math.radians(b[1])

    dLat = lat2 - lat1
    dLon = lon2 - lon1

    aux = (math.sin(dLat/2)**2) + (math.sin(dLon/2)**2) * math.cos(lat1) * math.cos(lat2)
    c = 2 * math.atan2(math.sqrt(aux), math.sqrt(1 - aux))
    return R * c


def way_to_km(nodes):
    if not isinstance(nodes, Iterable):
        raise TypeError
    if len(nodes) < 2:
        return 0
    idx = 0
    res = 0
    while idx + 1 < len(nodes):

        res += haversine(nodes[idx], nodes[idx + 1])
        idx += 1
    return res

