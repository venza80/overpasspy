from .execute import execute_QL
import overpass.cache


def get(area_id):
    area = overpass.cache.lookup("area", area_id)
    if area:
        return area
    query = "area({0});out meta;".format(area_id)
    return execute_QL(query)["elements"][0]


def get_many(area_ids):
    areas = []
    query = "("
    for area_id in area_ids:
        area = overpass.cache.lookup("area", area_id)
        if area:
            areas.append(area)
            continue
        else:
            query += "area({0});".format(area_id)

    if len(query) > 1:
        query += ");out meta;"
        areas += execute_QL(query)["elements"]
    return areas
