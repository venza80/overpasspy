from .execute import execute_QL
import overpass.cache


def get(rel_id):
    rel = overpass.cache.lookup("relation", rel_id)
    if rel:
        return rel
    query = "rel({0});out meta;".format(rel_id)
    return execute_QL(query)["elements"][0]


def get_many(rel_ids):
    rels = []
    query = "("
    for rel_id in rel_ids:
        rel = overpass.cache.lookup("relation", rel_id)
        if rel:
            rels.append(rel)
            continue
        else:
            query += "rel({0});".format(rel_id)

    if len(query) > 1:
        query += ");out meta;"
        rels += execute_QL(query)["elements"]
    return rels


def get_members(rel_id, member_type):
    ret = []
    query = "("
    rel = get(rel_id)
    for member in rel["members"]:
        if member["type"] in member_type:
            obj = overpass.cache.lookup(member["type"], member["ref"])
            if obj:
                ret.append(obj)
                continue
            else:
                if member["type"] == "node":
                    query += "node({0});".format(member["ref"])
                elif member["type"] == "way":
                    query += "way({0});".format(member["ref"])
                if member["type"] == "relation":
                    query += "rel({0});".format(member["ref"])
    if len(query) > 1:
        query += ");out meta;"
        ret += execute_QL(query)["elements"]
    return ret
