import urllib.request
import json
import time
import logging
log = logging.getLogger(__name__)

import overpass.cache

OVERPASS_URL = 'http://overpass.osm.rambler.ru/cgi/interpreter'

stats = {"num_queries": 0, "bytes_rx": 0}


def execute_QL(script, use_cache=True):
    if script[0] == '[':
        script = "[out:json]" + script
    else:
        script = "[out:json];" + script
    script = script.encode('utf-8')
    if len(script) > 60:
        log.debug("Query: {0}...".format(script[:57]))
    else:
        log.debug("Query: {0}".format(script))
    start_time = time.time()
    req = urllib.request.Request(OVERPASS_URL, script)
    try:
        response = urllib.request.urlopen(req)
    except urllib.request.HTTPError as e:
        log.error("HTTP Error: {0}".format(e.read()))
        return None
    else:
        data = response.read().decode("utf-8")
        end_time = time.time()
        log.debug("Query time: {0:.2f}".format(end_time - start_time))
        osm_data = json.loads(data)
        if "remark" in osm_data:
            log.error("HTTP Error: {0}".format(str(osm_data)))
            return None
    stats["num_queries"] += 1
    stats["bytes_rx"] += len(data)
    if use_cache:
        overpass.cache.put(osm_data)
    return osm_data

