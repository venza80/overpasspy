from .execute import execute_QL
import overpass.cache
import overpass.relations
import overpass.areas


def get(node_id):
    node = overpass.cache.lookup("node", node_id)
    if node:
        return node
    query = "node({0});out meta;".format(node_id)
    return execute_QL(query)["elements"][0]


def get_many(node_ids):
    nodes = []
    query = "("
    for node_id in node_ids:
        node = overpass.cache.lookup("node", node_id)
        if node:
            nodes.append(node)
            continue
        else:
            query += "node({0});".format(node_id)

    if len(query) > 1:
        query += ");out meta;"
        nodes += execute_QL(query)["elements"]
    return nodes


def is_in(node_id):
    query = "node({0});is_in;out ids;".format(node_id)
    area_ids = execute_QL(query, use_cache=False)["elements"]
    areas = overpass.areas.get_many([x["id"] for x in area_ids])
    is_in = [(x["tags"]["admin_level"], x["tags"]["name"]) for x in areas if "admin_level" in x["tags"] and int(x["tags"]["admin_level"]) <= 8]
    is_in = sorted(is_in, key=lambda x: x[0], reverse=True)
    return ", ".join([x[1] for x in is_in])


def get_parent_relations(node_id):
    query = "[out:json];node({0});rel(bn);out ids;".format(node_id)
    rel_ids = execute_QL(query, use_cache=False)["elements"]
    return overpass.relations.get_many([x["id"] for x in rel_ids])
