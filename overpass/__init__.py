from .cache import OverpassCache
import overpass.debug
import logging
log = logging.getLogger("overpass")

cache = None


def init():
    global cache
    if not cache:
        cache = OverpassCache()
    logging.basicConfig(level=logging.DEBUG)


def end():
    global cache
    overpass.debug.dump_stats()
    cache = None

