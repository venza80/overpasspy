def check_route_has_stops(route):
    num_stops = 0
    num_platforms = 0
    for m in route["members"]:
        if m["role"] == "stop":
            num_stops += 1
        elif m["role"] == "platform":
            num_platforms += 1

    if num_stops > 1 and num_platforms > 1:
        return None
    if num_stops == 1:
        return "Route has only one stop"
    if num_platforms == 1:
        return "Route has only one platform"
    if num_stops == 0:
        return "Route has no stops"
    if num_platforms == 0:
        return "Route has no platforms"
    return None

def check_old_tagging(route):
    for m in route["members"]:
        if "backward" in m["role"] or "forward" in m["role"]:
            return "Old public transport tagging"
    return None

