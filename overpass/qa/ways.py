def is_connected(ways):
    aux = {}
    closed_ways = []
    for way in ways:
        ns = way["nodes"][0]
        ne = way["nodes"][-1]
        if ns == ne:
            closed_ways += [way]
            continue
        if ns not in aux:
            aux[ns] = [way]
        else:
            aux[ns].append(way)
        if ne not in aux:
            aux[ne] = [way]
        else:
            aux[ne].append(way)

    # Try to fit orpahn nodes in closed ways
    for node_id in aux:
        if len(aux[node_id]) == 1:
            for cway in closed_ways:
                if node_id in cway["nodes"]:
                    aux[node_id].append(cway)

    error_list = []
    num_endpoints = 0
    for node_id in aux:
        if len(aux[node_id]) == 1:
            error_list.append((node_id, aux[node_id][0], "Disconnected node"))
            num_endpoints += 1
    if num_endpoints > 2:
        return error_list
    else:
        return []

