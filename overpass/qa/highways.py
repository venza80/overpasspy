def has_name(way):
    try:
        way["tags"]["name"]
        return []
    except KeyError:
        try:
            way["tags"]["ref"]
            return []
        except KeyError:
            return (way, "highway has neither name nor ref")

