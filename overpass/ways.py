from collections import Iterable

from .execute import execute_QL
from .distance import haversine
import overpass.cache
import overpass.nodes


def get(way_id):
    way = overpass.cache.lookup("way", way_id)
    if way:
        return way
    query = "way({0});out meta;".format(way_id)
    return execute_QL(query)["elements"][0]


def get_many(way_ids):
    ways = []
    query = "("
    for way_id in way_ids:
        way = overpass.cache.lookup("way", way_id)
        if way:
            ways.append(way)
            continue
        else:
            query += "way({0});".format(way_id)

    if len(query) > 1:
        query += ");out meta;"
        ways += execute_QL(query)["elements"]
    return ways


def get_all_nodes(way_ids):
    ''' Given a way_id returns all nodes belonging to it '''
    if not isinstance(way_ids, Iterable):
        way_ids = [way_ids]

    nodes = []
    query = ""
    for way_id in way_ids:
        way = get(way_id)
        count = overpass.cache.count_available("node", way["nodes"])
        if count < (len(way["nodes"]) / 2):
            query += "way({0});node(w);out meta;".format(way_id)
        else:
            nodes += overpass.nodes.get_many(way["nodes"])

    if len(query) > 0:
        nodes += execute_QL(query)["elements"]
    return nodes


def length_km(way):
    nodes = get_all_nodes(way["id"])
    nodes = dict([(x["id"], x) for x in nodes])
    idx = 0
    res = 0
    while idx + 1 < len(way["nodes"]):
        start = nodes[way["nodes"][idx]]
        end = nodes[way["nodes"][idx + 1]]
        res += haversine(start, end)
        idx += 1
    return res
