from .execute import execute_QL
import overpass.relations
import overpass.nodes


def get_all_lines_in_area(area_name, admin_levels="6|7|8", max_results=-1):
    admin_level = "[admin_level~\"%s\"]" % admin_levels
    query = 'area[name~"{0}"]{2};rel(area._);rel._["type"="route"];rel._["route"~"bus|tram|train|subway"];out ids{1};'
    if max_results > 0:
        max_results = " %d" % max_results
    else:
        max_results = ""
    query = query.format(area_name, max_results, admin_level)
    results = execute_QL(query, use_cache=False)
    lines_ids = results["elements"]
    return overpass.relations.get_many([x["id"] for x in lines_ids])


def get_all_bus_stops_fuzzy(name):
    query = 'node[public_transport="platform"][bus="yes"][name~"{0}"];out ids;node[highway="bus_stop"][name~"{0}",i];out ids;'
    query = query.format(name)
    stop_ids = execute_QL(query, use_cache=False)["elements"]
    return overpass.nodes.get_many([x["id"] for x in stop_ids])


def get_route_master(route_id):
    query = "rel({0});rel(br)[type=\"route_master\"];"
    query = query.format(route_id)
    masters = execute_QL(query)["elements"]
    if len(masters) == 1:
        return masters[0]  # should be the most common case
    else:
        return masters

