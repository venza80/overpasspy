import logging
log = logging.getLogger(__name__)
import memcache

# mc = memcache.Client(['127.0.0.1:11211'], debug=0)
#
# mc.set("some_key", "Some value")
# value = mc.get("some_key")
#
# mc.set("another_key", 3)
# mc.delete("another_key")
#
# mc.set("key", "1")   # note that the key used for incr/decr must be a string.
# mc.incr("key")
# mc.decr("key")


class OverpassCache:
    stats = {"hit": 0, "miss": 0, "added": 0, "updated": 0, "deleted": 0}

    def __init__(self):
        self.cache_available = False
        self.mc = memcache.Client(['127.0.0.1:11211'], debug=True)

    def lookup(self, osm_type, osm_id):
        osm_id = str(osm_id)
        obj = self.mc.get(osm_type + ":" + osm_id)
        if obj is None:
            self.stats["miss"] += 1
            return None
        else:
            self.stats["hit"] += 1
            return obj

    def put(self, overpass_reply):
        elems = [(e["type"] + ":" + str(e["id"]), e) for e in overpass_reply["elements"]]
        self.mc.set_multi(dict(elems), time=60 * 30)  # cache for half an hour
        self.stats["added"] += 1

